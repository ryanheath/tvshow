﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TvShow.Shows;
using TvShow.Shows.Models;

namespace TvShow.Controllers
{
    [Route("api/[controller]")]
    public class ShowsController : Controller
    {
        private readonly IShowsAdapter showsAdapter;

        public ShowsController(IShowsAdapter showsAdapter)
        {
            this.showsAdapter = showsAdapter;
        }

        [HttpGet]
        [ProducesResponseType(typeof(Show[]), 200)]
        [ProducesResponseType(404)]
        public async Task<IActionResult> GetShows([FromQuery]int page)
        {
            var shows = await showsAdapter.FetchShows(page);

            if (shows == null)
            {
                return NotFound();
            }

            return Ok(shows);
        }
    }
}
