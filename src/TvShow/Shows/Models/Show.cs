﻿namespace TvShow.Shows.Models
{
    public class Show
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public Person[] Cast { get; set; }
    }
}