﻿using System;
using Newtonsoft.Json;
using TvShow.Infrastructure;

namespace TvShow.Shows.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string Name { get; set; }
        [JsonConverter(typeof(DateOnlyConverter))]
        public DateTime? Birthday { get; set; }
    }
}
