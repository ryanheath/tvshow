﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TvShow.Shows.Models;
using TvShow.TvMaze;

namespace TvShow.Shows
{
    public class ShowsAdapter : IShowsAdapter
    {
        private readonly ITvMazeClient tvMazeClient;

        public ShowsAdapter(ITvMazeClient tvMazeClient)
        {
            this.tvMazeClient = tvMazeClient;
        }

        public async Task<Show[]> FetchShows(int page)
        {
            // we use a page of 10 items
            // tvMaze uses a page of 250 items
            var tvMazePage = page / 25;

            var tvMazeShows = await tvMazeClient.FetchShows(tvMazePage);

            if (tvMazeShows == null)
            {
                return null;
            }

            var shows = MapToShows(tvMazeShows, page);

            // get cast of each show in parallel
            var tasks = shows.Select(show => FetchCast(show.Id));
            var persons = await Task.WhenAll(tasks);

            for (var i = 0; i < shows.Length; i++)
            {
                shows[i].Cast = persons[i];
            }

            return shows;
        }

        public async Task<Person[]> FetchCast(int showId)
        {
            var cast = await tvMazeClient.FetchCast(showId);

            if (cast == null)
            {
                return new Person[0];
            }

            var persons = MapToPersons(cast);

            return persons;
        }

        private static Person[] MapToPersons(TvMaze.Models.Cast[] casts)
        {
            var qry = from cast in casts
                orderby cast.Person.Birthday.GetValueOrDefault(DateTime.MinValue) descending 
                select MapToPerson(cast.Person);

            return qry.ToArray();
        }

        private static Show[] MapToShows(TvMaze.Models.Show[] tvMazeShows, int page)
        {
            var startId = page * 10;
            var endId = startId + 10;

            var qry = from tvMazeShow in tvMazeShows
                where tvMazeShow.Id >= startId && tvMazeShow.Id < endId
                select MapToShow(tvMazeShow);

            return qry.ToArray();
        }

        private static Person MapToPerson(TvMaze.Models.CastPerson tvMazePerson)
        {
            // TODO: use AutoMapper when CastPerson gets much more properties

            return new Person
            {
                Id = tvMazePerson.Id,
                Name = tvMazePerson.Name,
                Birthday = tvMazePerson.Birthday
            };
        }

        private static Show MapToShow(TvMaze.Models.Show tvMazeShow)
        {
            // TODO: use AutoMapper when Show gets much more properties

            return new Show
            {
                Id = tvMazeShow.Id,
                Name = tvMazeShow.Name
            };
        }
    }
}
