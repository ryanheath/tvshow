﻿using System.Threading.Tasks;
using TvShow.Shows.Models;

namespace TvShow.Shows
{
    public interface IShowsAdapter
    {
        Task<Show[]> FetchShows(int page);
    }
}
