﻿using Newtonsoft.Json.Converters;

namespace TvShow.Infrastructure
{
    public class DateOnlyConverter : IsoDateTimeConverter
    {
        public DateOnlyConverter()
        {
            DateTimeFormat = "yyyy-MM-dd";
        }
    }
}
