﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TvShow.TvMaze.Models;

namespace TvShow.TvMaze
{
    public class TvMazeClient : ITvMazeClient
    {
        private readonly HttpClient hc;

        public TvMazeClient(HttpClient hc)
        {
            // TODO: use the upcoming HttpClientFactory in asp core 2.1

            this.hc = hc;
        }

        /// <inheritdoc />
        public Task<Show[]> FetchShows(int page) => Fetch<Show[]>($"http://api.tvmaze.com/shows?page={page}");

        /// <inheritdoc />
        public Task<Cast[]> FetchCast(int showId) => Fetch<Cast[]>($"http://api.tvmaze.com/shows/{showId}/cast");

        private async Task<T> Fetch<T>(string url)
        {
            while (true)
            {
                using (var response = await hc.GetAsync(url))
                {
                    switch (response.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            return default(T);

                        // Rate limiting
                        case (HttpStatusCode) 429:
                            await Task.Delay(5000);
                            continue;

                        default:
                            response.EnsureSuccessStatusCode();
                            break;
                    }

                    using (var content = response.Content)
                    {
                        var payload = await content.ReadAsStringAsync();
                        var cast = JsonConvert.DeserializeObject<T>(payload);

                        return cast;
                    }
                }
            }
        }
    }
}