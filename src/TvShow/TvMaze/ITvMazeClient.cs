﻿using System.Threading.Tasks;
using TvShow.TvMaze.Models;

namespace TvShow.TvMaze
{
    public interface ITvMazeClient
    {
        /// <summary>
        /// Returns the Shows in given page
        /// A page contains 250 shows at a maximum
        /// Return null when the page doesnt exist
        /// </summary>
        Task<Show[]> FetchShows(int page);

        /// <summary>
        /// Returns the cast of a show
        /// Return null when the show doesnt exist
        /// </summary>
        Task<Cast[]> FetchCast(int showId);
    }
}