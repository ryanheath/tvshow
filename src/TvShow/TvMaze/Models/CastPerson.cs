﻿using System;

namespace TvShow.TvMaze.Models
{
    public class CastPerson
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
    }
}
