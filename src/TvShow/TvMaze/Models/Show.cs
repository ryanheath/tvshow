﻿namespace TvShow.TvMaze.Models
{
    public class Show
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}