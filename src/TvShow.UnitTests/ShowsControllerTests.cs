using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Moq.AutoMock;
using TvShow.Controllers;
using TvShow.Shows;
using TvShow.Shows.Models;
using Xunit;

namespace TvShow.UnitTests
{
    public class ShowsControllerTests
    {
        private readonly ShowsController controller;
        private readonly Mock<IShowsAdapter> adapter;

        public ShowsControllerTests()
        {
            var autoMocker = new AutoMocker();
            adapter = autoMocker.GetMock<IShowsAdapter>();
            controller = autoMocker.CreateInstance<ShowsController>();
        }

        public class GetShows : ShowsControllerTests
        {
            [Fact]
            public async void ShouldReturn404WhenPageDoesNotExist()
            {
                adapter.Setup(x => x.FetchShows(0)).ReturnsAsync((Show[])null);

                var result = await controller.GetShows(0);

                result.Should().BeOfType<NotFoundResult>();
            }

            [Fact]
            public async void ShouldReturnOkResultWhenPageDoesExist()
            {
                var shows = new Show[0];
                adapter.Setup(x => x.FetchShows(3)).ReturnsAsync(shows);

                var result = await controller.GetShows(3);

                result.Should().BeOfType<OkObjectResult>();
                var resultShows = result.As<OkObjectResult>().Value.As<Show[]>();
                resultShows.Should().BeSameAs(shows);
            }
        }
    }
}
