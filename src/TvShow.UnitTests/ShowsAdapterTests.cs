using System;
using FluentAssertions;
using Moq;
using Moq.AutoMock;
using TvShow.Shows;
using TvShow.TvMaze;
using Xunit;

namespace TvShow.UnitTests
{
    public class ShowsAdapterTests
    {
        private readonly ShowsAdapter adapter;
        private readonly Mock<ITvMazeClient> tvClient;

        public ShowsAdapterTests()
        {
            var autoMocker = new AutoMocker();
            tvClient = autoMocker.GetMock<ITvMazeClient>();
            adapter = autoMocker.CreateInstance<ShowsAdapter>();
        }

        public class FetchShows : ShowsAdapterTests
        {
            [Fact]
            public async void ShouldReturnNullWhenPageDoesNotExist()
            {
                tvClient.Setup(x => x.FetchShows(0)).ReturnsAsync((TvMaze.Models.Show[])null);

                var result = await adapter.FetchShows(0);

                result.Should().BeNull();
            }

            [Fact]
            public async void ShouldReturn10ItemsMaxPerPage()
            {
                tvClient.Setup(x => x.FetchShows(0)).ReturnsAsync(FakeTvMazeShows(250));

                var shows = await adapter.FetchShows(3);

                shows.Should().NotBeNull();
                shows.Length.Should().Be(10);
                shows[0].Id.Should().Be(30);
                shows[9].Id.Should().Be(39);
            }

            [Fact]
            public async void ShouldReturn0ItemsForAnEmptyPage()
            {
                tvClient.Setup(x => x.FetchShows(0)).ReturnsAsync(FakeTvMazeShows(0));

                var shows = await adapter.FetchShows(3);

                shows.Should().NotBeNull();
                shows.Length.Should().Be(0);
            }

            [Fact]
            public async void ShouldReturnCastPerShow()
            {
                tvClient.Setup(x => x.FetchShows(0)).ReturnsAsync(FakeTvMazeShows(40));
                for (var i = 0; i < 10; i++)
                {
                    var id = i + 30;
                    tvClient.Setup(x => x.FetchCast(id)).ReturnsAsync(FakeTvMazeCast(3));
                }

                var shows = await adapter.FetchShows(3);

                shows.Should().NotBeNull();
                shows.Length.Should().Be(10);
                for (var i = 0; i < 10; i++)
                {
                    shows[i].Cast.Should().NotBeNull();
                    shows[i].Cast.Length.Should().Be(3);
                }
            }

            private static TvMaze.Models.Show[] FakeTvMazeShows(int count)
            {
                var shows = new TvMaze.Models.Show[count];

                for (var i = 0; i < count; i++)
                {
                    shows[i] = new TvMaze.Models.Show
                    {
                        Id = i + 1,
                        Name = i.ToString()
                    };
                }

                return shows;
            }
        }

        public class FetchPersons : ShowsAdapterTests
        {
            [Fact]
            public async void ShouldReturnEmptyArrayWhenCastDoesNotExist()
            {
                tvClient.Setup(x => x.FetchCast(0)).ReturnsAsync((TvMaze.Models.Cast[])null);

                var result = await adapter.FetchCast(0);

                result.Should().NotBeNull();
                result.Length.Should().Be(0);
            }

            [Fact]
            public async void ShouldReturnPersons()
            {
                tvClient.Setup(x => x.FetchCast(3)).ReturnsAsync(FakeTvMazeCast(5));

                var persons = await adapter.FetchCast(3);

                persons.Should().NotBeNull();
                persons.Length.Should().Be(5);
                persons[0].Id.Should().Be(5);
                persons[4].Id.Should().Be(1);
            }

            [Fact]
            public async void ShouldReturnPersonsInDescOrderBirthday()
            {
                tvClient.Setup(x => x.FetchCast(3)).ReturnsAsync(FakeTvMazeCast(5));

                var persons = await adapter.FetchCast(3);

                persons.Should().NotBeNull();
                persons.Length.Should().Be(5);
                persons[0].Birthday.Value.Year.Should().BeGreaterThan(persons[1].Birthday.Value.Year);
                persons[1].Birthday.Value.Year.Should().BeGreaterThan(persons[2].Birthday.Value.Year);
                persons[2].Birthday.Value.Year.Should().BeGreaterThan(persons[3].Birthday.Value.Year);
                persons[3].Birthday.Value.Year.Should().BeGreaterThan(persons[4].Birthday.Value.Year);
            }

            [Fact]
            public async void ShouldReturnPersonsInDescOrderBirthdayWithNoBirthdayValue()
            {
                var cast = FakeTvMazeCast(5);
                cast[2].Person.Birthday = null;
                tvClient.Setup(x => x.FetchCast(3)).ReturnsAsync(cast);

                var persons = await adapter.FetchCast(3);

                persons.Should().NotBeNull();
                persons.Length.Should().Be(5);
                persons[0].Birthday.Value.Year.Should().BeGreaterThan(persons[1].Birthday.Value.Year);
                persons[1].Birthday.Value.Year.Should().BeGreaterThan(persons[2].Birthday.Value.Year);
                persons[2].Birthday.Value.Year.Should().BeGreaterThan(persons[3].Birthday.Value.Year);
                persons[4].Birthday.Should().BeNull();
            }
        }

        private static TvMaze.Models.Cast[] FakeTvMazeCast(int count)
        {
            var casts = new TvMaze.Models.Cast[count];

            for (var i = 0; i < count; i++)
            {
                casts[i] = new TvMaze.Models.Cast
                {
                    Person = new TvMaze.Models.CastPerson
                    {
                        Id = i + 1,
                        Name = i.ToString(),
                        Birthday = new DateTime(1980 + i, 1, 1)
                    }
                };
            }

            return casts;
        }
    }
}
