using System;
using System.Net.Http;
using Newtonsoft.Json;
using Xunit;
using FluentAssertions;

namespace TvShow.UnitTests
{
    public class TvMazeTests
    {
        class Show
        {
            public int Id { get; set; }
            public string Name { get; set; }
        }


        [Fact(Skip = "Prototyping, not needed anymore")]
        public async void CanGetShows()
        {
            var hc = new HttpClient();

            var response = await hc.GetStringAsync("http://api.tvmaze.com/shows?page=1");
            var shows = JsonConvert.DeserializeObject<Show[]>(response);

            shows.Length.Should().BeGreaterThan(200);
            shows[0].Id.Should().BeGreaterThan(0);
            shows[0].Name.Should().NotBeNullOrWhiteSpace();
        }

        public class Cast
        {
            public CastPerson Person { get; set; }

            public class CastPerson
            {
                public int Id { get; set; }
                public string Name { get; set; }
                public DateTime? Birthday { get; set; }
            }
        }

        [Fact(Skip = "Prototyping, not needed anymore")]
        public async void CanGetCast()
        {
            var hc = new HttpClient();

            var response = await hc.GetStringAsync("http://api.tvmaze.com/shows/1/cast");
            var cast = JsonConvert.DeserializeObject<Cast[]>(response);

            cast.Length.Should().BeGreaterThan(2);
            var person = cast[0].Person;
            person.Should().NotBeNull();
            person.Id.Should().BeGreaterThan(0);
            person.Name.Should().NotBeNullOrWhiteSpace();
            person.Birthday.Value.Year.Should().BeGreaterThan(1800);
        }
    }
}
