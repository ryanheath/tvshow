using System;
using System.Net.Http;
using FluentAssertions;
using TvShow.TvMaze;
using Xunit;

namespace TvShow.UnitTests
{
    public class TvMazeClientTests
    {
        private readonly TvMazeClient tvMazeClient;

        public TvMazeClientTests()
        {
            tvMazeClient = new TvMazeClient(new HttpClient());
        }

        public class FetchShows : TvMazeClientTests
        {
            [Fact]
            public async void ShouldReturnNullWhenPageDoesNotExist()
            {
                var result = await tvMazeClient.FetchShows(10000);

                result.Should().BeNull();
            }

            [Fact]
            public async void ShouldReturnArrayOfShows()
            {
                var result = await tvMazeClient.FetchShows(0);

                result.Should().NotBeNull();
                result.Length.Should().BeGreaterThan(0);
            }

            [Fact]
            public async void ShouldReturnShowsWithProperties()
            {
                var result = await tvMazeClient.FetchShows(0);

                result[0].Id.Should().BeGreaterThan(0);
                result[0].Name.Should().NotBeNullOrWhiteSpace();
            }
        }

        public class FetchCast : TvMazeClientTests
        {
            [Fact]
            public async void ShouldReturnNullWhenShowDoesNotExist()
            {
                var result = await tvMazeClient.FetchCast(0);

                result.Should().BeNull();
            }

            [Fact]
            public async void ShouldReturnArrayOfCast()
            {
                var result = await tvMazeClient.FetchCast(1);

                result.Should().NotBeNull();
                result.Length.Should().BeGreaterThan(0);
            }

            [Fact]
            public async void ShouldReturnPersonsWithProperties()
            {
                var result = await tvMazeClient.FetchCast(1);

                var person = result[0].Person;

                person.Id.Should().BeGreaterThan(0);
                person.Name.Should().NotBeNullOrWhiteSpace();
                person.Birthday.Value.Year.Should().BeGreaterThan(1800);
            }

            [Fact]
            public async void ShouldNotThrowWhenCalledAbundantly()
            {
                var rnd = new Random();

                for (var i = 1; i < 100; i++)
                {
                    await tvMazeClient.FetchCast(rnd.Next(1, 1000));
                }
            }
        }
    }
}
