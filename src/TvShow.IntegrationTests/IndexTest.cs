using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using FluentAssertions;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using Xunit;

namespace TvShow.IntegrationTests
{
    public class ApiShowsTest
    {
        private readonly TestServer testServer;
        private readonly HttpClient hc;

        public ApiShowsTest()
        {
            testServer = new TestServer(WebHost.CreateDefaultBuilder()
                .UseStartup<Startup>()
                .UseEnvironment("Development"));
            hc = testServer.CreateClient();
        }

        [Fact]
        public async void RootShouldReturn404()
        {
            var response = await hc.GetAsync("/");

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void ShowsShouldReturnAtLeast10Items()
        {
            var response = await hc.GetAsync("/api/shows");


            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();

            var shows = JsonConvert.DeserializeObject<Show[]>(content);
            shows.Should().NotBeNull();
            shows.Length.Should().BeGreaterThan(1);
            shows.Length.Should().BeLessOrEqualTo(10);
            shows[0].Id.Should().BeGreaterThan(0);
            shows[0].Name.Should().NotBeNullOrWhiteSpace();
            shows[0].Cast.Should().NotBeNull();
            shows[0].Cast.Length.Should().BeGreaterThan(0);
            shows[0].Cast[0].Id.Should().BeGreaterThan(0);
            shows[0].Cast[0].Name.Should().NotBeNullOrWhiteSpace();
            shows[0].Cast[0].Birthday.Should().NotBeNullOrWhiteSpace();
        }

        [Fact]
        public async void ShowsPage3ShouldReturnItemsWithId30Till40()
        {
            var response = await hc.GetAsync("/api/shows?page=3");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();

            var shows = JsonConvert.DeserializeObject<Show[]>(content);
            shows.Should().NotBeNull();
            shows.Length.Should().BeGreaterThan(1);
            shows.Length.Should().BeLessOrEqualTo(10);
            shows.All(s => s.Id >= 30 && s.Id < 40).Should().BeTrue();
        }

        [Fact]
        public async void ShowsPage10000ShouldReturn404()
        {
            var response = await hc.GetAsync("/api/shows?page=10000");

            response.StatusCode.Should().Be(HttpStatusCode.NotFound);
        }

        [Fact]
        public async void ShowsShouldHavePersonsBirthdayWithoutTime()
        {
            var response = await hc.GetAsync("/api/shows");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            var content = await response.Content.ReadAsStringAsync();

            var shows = JsonConvert.DeserializeObject<Show[]>(content);
            shows.All(s => s.Cast.All(c => c.Birthday == null || c.Birthday.Length == 10)).Should().BeTrue();
        }

        [Fact]
        public async void ShowsShouldGzipContent()
        {
            hc.DefaultRequestHeaders.AcceptEncoding.Add(new StringWithQualityHeaderValue("gzip"));
            var response = await hc.GetAsync("/api/shows");

            response.StatusCode.Should().Be(HttpStatusCode.OK);
            response.Content.Headers.ContentEncoding.Contains("gzip").Should().BeTrue();
        }

        class Show
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public Person[] Cast { get; set; }

            public class Person
            {
                public int Id { get; set; }
                public string Name { get; set; }
                public string Birthday { get; set; }
            }
        }
    }
}
