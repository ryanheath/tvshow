# TvShow api

TvShow api returns all known TV shows and their casts.

The data is used from [TVMaze](https://www.tvmaze.com/).

---

# Use the swagger file

Clone, build and start the project.

Browse to the [swagger file](http://localhost:27872/swagger/) to see and try out the api.

---

Happy browsing!

// Ryan